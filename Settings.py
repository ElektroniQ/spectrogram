from tkinter import *
from tkinter import ttk
from enum import Enum

class SettingsEvent(Enum):
    CURSOR_CHANGE = 1

class Settings:
    def __init__(self, root):
        self.root = root
        self.width = 300
        self.height = 300

        self.sample_number = 100
        self.window = "boxcar"
        self.apply_callback = None

    def Init(self):
        self.frame = Frame(self.root, width=self.width, height=self.height)

        # Filtering
        self.label_window = Label(self.frame, text="Window:")
        self.label_window.grid(row = 0, column=0, sticky = W, padx = 5, pady = 5)

        self.input_window = ttk.Combobox(self.frame, 
                            values=[
                                    "boxcar",
                                    "triang",
                                    "blackman",
                                    "hamming",
                                    "hann",
                                    "bartlett",
                                    "flattop",
                                    "parzen",
                                    "bohman",
                                    "blackmanharris",
                                    "nuttall",
                                    "barthann"])
        self.input_window.current(1)
        self.input_window.grid(row = 0, column=1, sticky = W)

        # Sample number
        self.label_sample_number = Label(self.frame, text="Sample number:")
        self.label_sample_number.grid(row = 1, column=0, sticky = W, padx = 5, pady = 5)

        self.input_sample_number = Entry(self.frame, text=self.sample_number)
        self.input_sample_number.insert(0, "%d" % self.sample_number)
        self.input_sample_number.grid(row = 1,column = 1, sticky = E+W)

        # Apply
        self.apply_button = Button(self.frame, text="Apply", command=self.__Apply)
        self.apply_button.grid(row = 10, column=0, columnspan=2, sticky = W+E, padx = 5, pady = 5)

        self.Refresh()

    def GetObject(self):
        return self.frame

    def Refresh(self):
        self.input_sample_number.delete(0,100)

        self.input_sample_number.insert(0, "%d" % self.sample_number)
        

    def __Apply(self):
        self.sample_number = int(self.input_sample_number.get())
        self.window = self.input_window.get()
        self.Refresh()
        self.apply_callback()
